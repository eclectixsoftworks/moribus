﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnitBase
{
	public float Motivation;
	public float Health;
	public float Food;
	public float Water;
}
