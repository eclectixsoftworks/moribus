﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControl : UnitBaseController 
{
	private float aiSpeed;
	enum States
	{
		Patrol,
		Idle
	};

	void Start()
	{
		aiSpeed  = (Unit.Health + Unit.Motivation) / 2.0f;
		Unit = new UnitBase ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Move ();
	}

	public override void Move ()
	{
		transform.Translate (Vector2.right * aiSpeed * Time.deltaTime);
	}

	private void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Player" || coll.gameObject.tag == "Obstacle") 
		{
			transform.Rotate (new Vector3 (0.0f, 180.0f, 0.0f));
		}
	}
}
