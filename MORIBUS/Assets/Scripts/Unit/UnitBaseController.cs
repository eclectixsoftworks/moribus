﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBaseController : MonoBehaviour {

	public UnitBase Unit;
	public Camera Cam;
	private Vector3 targetPos;
	private Rigidbody2D rb2d;
	private bool isGrounded;

	private float playerSpeed;
	void Start () 
	{
		// assuming this is how it will be
		targetPos = transform.position;
		playerSpeed = (Unit.Health + Unit.Motivation) / 2.0f;
		rb2d = GetComponent<Rigidbody2D> ();
		Unit = new UnitBase ();
	}
	void Update () 
	{
		Move ();
		Cam.transform.position = new Vector3(transform.position.x + 3.0f, Cam.transform.position.y, Cam.transform.position.z);
	}

	public virtual void Move()
	{
		if (Input.GetKey (KeyCode.A))
		{
			transform.Translate (Vector2.right * -playerSpeed * Time.deltaTime);
		} 

		else if (Input.GetKey (KeyCode.D)) 
		{
			transform.Translate (Vector2.right * playerSpeed * Time.deltaTime);
		}
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (Input.GetKeyDown (KeyCode.W))
			Debug.Log ("Should use door");
	}
}
