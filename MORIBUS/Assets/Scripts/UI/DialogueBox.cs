﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour 
{
	DialogueActor dialogueActor;
	Animator animator;
	public Text UItext;
	public Button next;
	public Button skip;
	string text;

	void Start()
	{
		animator = GetComponent<Animator>();
	}

	public void SetText(string display)
	{
		UItext.text = display;
	}

	public void PlayAnim()
	{
		animator.SetTrigger("DialogueTrigger");
	}

	public void ExitAnim()
	{
		animator.SetTrigger("DialogueTrigger");
	}
	

}
