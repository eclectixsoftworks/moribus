﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Choice : MonoBehaviour 
{
	public UnityEvent choicePicked;

	public string choiceText;

	public virtual void ChoiceAction()
	{
		
	}

}
