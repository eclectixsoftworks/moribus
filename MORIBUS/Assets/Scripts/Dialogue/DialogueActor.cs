﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueActor : MonoBehaviour {
	public Dialogue 	dialogueCurrent;
	public DialogueList dialogueList;
	public ChoiceList 	choiceList;

	public int dialogueIndex = -1;
	int choiceSetIndex = -1;



	DialogueSession dialogueSession;

	void Start()
	{
		dialogueSession = DialogueSession.instance;
	}

	public void DialogueNext()
	{
		dialogueIndex++;
		if (dialogueIndex > dialogueList.dialogues.Count - 1) dialogueIndex = dialogueList.dialogues.Count - 1;
		dialogueCurrent = dialogueList.dialogues[dialogueIndex];
		dialogueSession.currentDialogueActor = this;
		dialogueSession.StartDialogue ();
	}

	public void ChoiceNext()
	{
		
	}

	public void DialogueEnd()
	{
		
	}
}
