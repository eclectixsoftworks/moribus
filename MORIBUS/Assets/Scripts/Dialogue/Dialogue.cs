﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[CreateAssetMenu(fileName = "Dialogue", menuName = "Dialogue/Dialogue")]
[System.Serializable]
public class Dialogue : ScriptableObject 
{
	

	public bool dialogueLock;
	public bool dialogueFinished;
	public bool hasChoice;

	public ChoiceSet choiceSet;

	public string dialogueName;
	[TextArea(0, 10)]
	public List<string> dialogueText;
}
