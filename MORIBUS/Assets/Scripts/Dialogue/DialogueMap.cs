﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "DialogueList", menuName = ("Dialogue/DialogueMap"))]
public class DialogueMap : ScriptableObject {

	public string dialogueOwner;
	public List<DialogueList> dialogueLists;
}
