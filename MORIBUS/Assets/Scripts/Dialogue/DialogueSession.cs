﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueSession : MonoBehaviour 
{
	public DialogueActor currentDialogueActor;

	public string 	display {get; private set;}
	Queue<string> 	text;
	string 			currentLine;

	CanvasController 	canvasController;
	DialogueBox 		dialogueBox;

	public static DialogueSession instance;

	public UnityEvent dialoguePlaying;
	public UnityEvent dialogueStarted;
	public UnityEvent dialogueNext;
	public UnityEvent dialogueEnded;
	public UnityEvent dialogueUnlocked;
	public UnityEvent dialogueShowChoice;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		canvasController = CanvasController.instance;
		dialogueBox = canvasController.dialogueBox.GetComponent<DialogueBox>();
		dialogueBox.next.onClick.AddListener(DisplayNextSentence);
		dialogueBox.skip.onClick.AddListener(SkipTyping);


	}



	//Plays dialogue
	public void StartDialogue()
	{
		text = new Queue<string>();
		foreach (string sentence in currentDialogueActor.dialogueCurrent.dialogueText)
		{
			text.Enqueue(sentence);
		}
		dialogueBox.PlayAnim();
		DisplayNextSentence();

	}
	public void DisplayNextSentence()
	{
		if (text.Count == 0 && currentDialogueActor.dialogueCurrent.hasChoice == false)
		{
			dialogueBox.ExitAnim();
			return;
		}

		StopAllCoroutines();
		currentLine = text.Dequeue();
		StartCoroutine(TypeText(currentLine));
	}

	public void SkipTyping()
	{
		StopAllCoroutines();
		display = currentLine;
		dialogueBox.SetText(display);

	}
	//Starts typing the dialogue
	public IEnumerator TypeText(string text)
	{
		display = "";
		foreach (char letter in text.ToCharArray())
		{
			//Use display to get dialogue letter per letter
			display += letter;
			dialogueBox.SetText(display);
			yield return new WaitForSeconds(0f);
		}
	}

}
