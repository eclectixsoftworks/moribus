﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DialogueList", menuName = ("Dialogue/DialogueList"))]
public class DialogueList : ScriptableObject 
{
	public string dialogueOwner;
	public List<Dialogue> dialogues;
}
