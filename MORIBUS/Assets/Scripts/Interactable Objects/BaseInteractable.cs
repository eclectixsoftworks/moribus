﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BaseInteractable : MonoBehaviour 
{
	public float range;
	public float distance;
	bool isHighestPriority;
	public bool isInteractable;
	public bool hasDialogue;
	public UnityEvent inRange;
	public UnityEvent outRange;
	public UnityEvent interacted;
	public UnityEvent interactable;


	public GameObject player;

	void FixedUpdate()
	{
		distance = (Vector3.Distance(player.transform.position, this.gameObject.transform.position));
		if (Vector3.Distance(player.transform.position, this.gameObject.transform.position) < range)
		{
			inRange.Invoke();
			isInteractable = true;
		}
		if (Vector3.Distance(player.transform.position, this.gameObject.transform.position) > range)
		{
			outRange.Invoke();
			isInteractable = false;
		}
		if (Input.GetKeyDown(KeyCode.E) && isInteractable == true)
		{
			Interact();
		}
	}

	public virtual void Interact()
	{
		Debug.Log("Interactable");
		interacted.Invoke();
	}
}
