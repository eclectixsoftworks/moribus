﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractablePopup : MonoBehaviour 
{
	public Image image;
	
	void Start()
	{
		
	}
	
	public void ShowSprite()
	{
		Color tmp = image.color;
		tmp.a = 255;
		image.color = tmp;
	}
	public void HideSprite()
	{
		Color tmp = image.color;
		tmp.a = 0;
		image.color = tmp;
	}

}
