﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteractable : BaseInteractable 
{
	public GameObject destination;
	
	
	void Start()
	{
		//interacted.AddListener(SendToDestination);

	}

	public IEnumerator SendToDestination()
	{
		yield return new WaitForSeconds(0.1f);
		player.transform.position = destination.gameObject.transform.position;
		Debug.Log(this);
	}

	public override void Interact()
	{
		base.Interact();
		StartCoroutine( SendToDestination());
	}

}
