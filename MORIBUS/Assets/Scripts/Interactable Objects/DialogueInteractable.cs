﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueInteractable : BaseInteractable {
	CanvasController canvasController;
	DialogueActor dialogueActor;
	void Start () 
	{
		canvasController = CanvasController.instance;
		dialogueActor = GetComponent<DialogueActor>();
		interacted.AddListener(dialogueActor.DialogueNext);
	}
	
	public override void Interact()
	{
		//dialogueActor.StartDialogue();
		base.Interact();
	}
}
